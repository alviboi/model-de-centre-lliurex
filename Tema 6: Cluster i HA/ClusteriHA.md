---
title: "CLUSTER I HA"
author: [Alfredo Rafael Vicente Boix i Javier Estellés Dasi]
date: "2020-11-25"
subject: "Proxmox"
keywords: [Xarxa, Instal·lació]
subtitle: "Exemple d'esquema de xarxa en el model de centre"
lang: "es"
page-background: "background10.pdf"
titlepage: true,
titlepage-rule-color: "360049"
titlepage-background: "background10.pdf"
colorlinks: true
toc-own-page: true
header-includes:
- |
  ```{=latex}
  \usepackage{awesomebox}
  \usepackage{caption}
  \usepackage{array}
  \usepackage{tabularx}
  \usepackage{ragged2e}
  \usepackage{multirow}


  ```
pandoc-latex-environment:
  noteblock: [note]
  tipblock: [tip]
  warningblock: [warning]
  cautionblock: [caution]
  importantblock: [important]
...

<!-- \awesomebox[violet]{2pt}{\faRocket}{violet}{Lorem ipsum…} -->

# Introducció

En aquesta última unitat, explicarem com muntar un clúster en PROXMOX. És un procediment molt
senzill, però s’han de tenir en compte les següents consideracions:

* En muntar un clúster pot tenir diferents màquines entre els hipervisors del clúster.
* Si no muntem un sistema d’alta disponibilitat, no té massa sentit.
* Per a muntar un sistema d’alta disponibilitat cal muntar un CEPH (no s’explica en aquest curs) o
    tenir una cabina externa.

:::warning
Les cabines externes són sistemes molt fiables i difícilment fallen, ja que només serveixen dades. Tot el processament es faria al sistema PROXMOX.
:::

:::warning
Un sistema d’alta disponibilitat permetrà que quan una màquina deixe de funcionar immediatament altre hipervisor se n’adonarà que alguna cosa està passant i la posarà en marxa, sense necessitat que cap persona intervinga.
:::

# Muntar clúster en PROXMOX

Ja que no deposam de tres ordinadors, es pot muntar un sistema d’alta disponibilitat en Virtualbox per a veure el seu funcionament. La màquina utilitzada és un Ryzen 5 amb 8 GB de RAM i un NAS. S’ha muntat el següent sistema:

| Màquina | Característiques |
| --- | ------|
| Proxmox 1 | Proxmox al Virtualbox |
| Proxmox 2 | Proxmox al Virtualbox |
| Proxmox 3 | Proxmox al Virtualbox |
| TrueNAS | Muntat a una màquina externa |


:::warning
La quantitat mínima d’hipervisors per a poder fer funcionar un sistema d’alta disponibilitat és de 3. Es necessita per a fer votació per saber qui està viu, qui guanye amb dos vots (ell i altre) està viu, això significa tenir quorum , qui tinga només un vot està mort (significaria que no té accés a la xarxa).
:::

El primer que cal fer és tenir 3 servidors instal·lats amb PROXMOX. En aquest cas hem muntat els 3 hipervisors amb una única targeta de xarxa. Si es muntara el model de centre tots 3 hipervisors, haurien de tenir la mateixa configuració de xarxa a excepció de la seua ip, òbviament.

![Màquines virtuals al Virtualbox](media_es/1.png)  

::tip
Recordeu que és necessari que cada PROXMOX tinga un hostname diferent. Si el nom
del node coincidix cal canviar-lo. Es deu modificar a l’arxiu /etc/hostname, i a /etc/hosts.
:::

Una vegada tenim els 3 hipervisors funcionant. Clickem sobre el datacenter i anem **Cluster** i clickem a **Create cluster**.

![Creació de cluster](media_es/2.png)  

Una vegada polsem a **Create cluster** ens apareixerà la següent finestra on li donarem el nom al clúster que vulguem:


![Nom del cluster](media_es/3.png)  

Quan posem a **Create** ens apareixerà la següent finestra:

![Creació de cluster amb èxit](media_es/4.png)  

Com podem veure en aquests moments només tindrem un hipervisor al nostre cluster. Si volem afegir
més hipervisors clickarem sobre **Join information**:


![Membres del cluster](media_es/5.png)  

I li donarem a **Copy information**. Aquests són els paràmetres que utilitzarà PROXMOX per a afegir els altres hipervisors al clúster.

![Informació a passar a altres hipervisors](media_es/6.png)  

Canviem d’hipervisor i anem novament a Clúster en el nostre Datacenter:

![Unir-se a un clúster](media_es/7.png)  

Polsarem a **Join cluster** i pegarem la informació copiada del primer hipervisor:

![Pegar informació](media_es/8.png)  

Al clicar sobre **Join cluster** ens apareixerà la següent informació:

![Unió al clúster amb èxit](media_es/9.png)  

Aquest procés es repetirà per a altre hipervisor:

![Tercer hipervisor](media_es/10.png)  


![Unió al clúster amb èxit](media_es/11.png)  

Ara podrem veure com tenim els tres hipervisors al mateix clúster:

![Relació d’hipervisors al clúster](media_es/12.png)  


En aquests moments ja tenim el nostre clúster muntat.

# Muntatge d’alta disponibilitat

Per a realitzar el nostre muntatge d’alta disponibilitat utiltizarem una cabina externa. Per a fer les proves s’ha utilitzat la distro Tiny Core, la versió de 16 Mb. Es tracta d’una distro que a penes consumeix recursos i es poden fer proves amb ella en virtualització anidada.

:::warning
Si es decidix fer el muntatge d’alta disponibilitat amb una cabina, cal disposar d’una
cabina amb 4 targetes de xarxa per a muntar un bond, o d’una targeta de 10Gb que
obligaria a tenir un switch que suportara 10 Gb.
:::

## NAS i màquina virtual

En primer lloc afegim el nostre NAS al Proxmox. Quan l’afegim al datacenter serà visible per part de tots els hipervisors.

![Afegir NAS](media_es/13.png)  

Pugem la nostra ISO (no tenim el perquè fer-ho al NAS)

![Pujar ISO](media_es/14.png)  

I creem la nostra màquina virtual:

![Creació de màquina virtual](media_es/15.png)

![Creació de màquina virtual](media_es/16.png)  

![Creació de màquina virtual](media_es/17.png)  

No cal donar molta memòria a aquesta màquina:

![Creació de màquina virtual](media_es/18.png)  

I arranquem la màquina:

![Màquina en funcionament](media_es/19.png)  


La màquina arranca de seguida i funciona molt fluida:

#3.2 Alta disponibilitat

Una vegada tenim la màquina funcionant anirem a Datacenter i després HA i clicarem sobre **Add**.

![Alta disponibilitat](media_es/20.png)  

Ens apareixerà la següent finestra on haurem d’escollir la màquina que volem tenir funcionant en tot moment:

![Escollir màquina](media_es/21.png)  

![Escollir màquina](media_es/22.png)  


En **Request state** cal tenir estat started.

I ja tenim el nostre sistema muntat:

![Llistat de màquines amb Alta disponibilitat](media_es/23.png)  

Amb Virtualbox podem provar d’apagar una màquina per a veure què passa:


![Prova de pèrdua d’un hipervisor](media_es/24.png)  

Al cap del temps podem veure com el nostre hipervisor ja no està en marxa i com la màquina ha canviat d’hipervisor i ara està funcionant a l’hipervisor 2:


![Màquina que ha passat automàticament a altre hipervisor](media_es/25.png)  

Podem veure que la màquina està funcionant:

![Tiny Core funcionant a altre hipervisor](media_es/26.png)  


:::warning
No espereu que la màquina es pose en marxa de seguida, el procés pot trigar uns 5 minuts i si hi han clients semilleugers funcionant, probablement deixaran de funcionar correctament. En aquest procés pot haver-hi certa pèrdua d’informació ja que no es recupera l’estat de la màquina.
:::

# Consideracions finals

Es pot tractar de tenir un sistema on hi haja una mínima pèrdua d’informació amb dos hipervisors i si no disposes de NAS. Es podrien tenir dues màquines duplicades amb dos hipervisors i utilitzar la replicació.

Es podria tenir el /net a un disc virtual a banda i que aquest anara replicant-se cada 30 min entre els dos hipervisors, si un d’ells caiguera. Es podria posar en marxa l’altra màquina a l’altre hipervisor i, com a molt, s’haurien perdut 30 minuts d’informació.

Podrien configurar-ho dins de la màquina virtual a l’apartat de **Replication**.

![Replicació de discos](media_es/27.png)  

Es podrien fer moltes més coses, ja que PROXMOX permet automatitzar moltes tasques i tenir un munt de serveis. Amb aquest curs hem tractat de donar-vos unes pinzellades sobre el que es pot fer i com s’està utilitzant als centres educatius.


